package com.benas.branch;

import java.util.ArrayList;
import java.util.List;

public class Branch {

    private Branch parent;
    private List<Branch> children = new ArrayList<>();
    private List<Branch> leafs = new ArrayList<>();


    public Branch(Branch parent) {
        this.parent = parent;
    }

    //method to add children nodes and set parent values for them
    public void addChildren(int howManyChildren, Branch parentBranch) {
        for (int i = 0; i < howManyChildren; i++) {
            this.children.add(new Branch(parentBranch));
        }
    }

    //method to collect all leafs (nodes without children nodes) and add them to 'leafs' list
    public void collectAllLeafs(Branch rootBranch) {

        for (Branch branch : rootBranch.getChildren()) {

            if (branch.getChildren().isEmpty()) {
                this.leafs.add(branch);
            }
            collectAllLeafs(branch);
        }
    }

    //method to calculate depths for every single leaf and add them to 'leafsDepths' list
    public List<Integer> calculateDepth(List<Branch> theLeafs) {
        List<Integer> leafsDepths = new ArrayList<>();

        for (int i = 0; i < theLeafs.size(); i++) {
            int occurrences = countOccurrences(theLeafs.get(i));
            leafsDepths.add(occurrences);
        }
        return leafsDepths;
    }

    //helper method to calculate depth (by calculating parent nodes)
    public int countOccurrences(Branch branch) {
        int count = 1;
        if (branch.getParent() != null) {
            count = count + (countOccurrences(branch.getParent()));
        }
        return count;
    }


    public Branch getParent() {
        return parent;
    }

    public List<Branch> getChildren() {
        return children;
    }

    public List<Branch> getLeafs() {
        return leafs;
    }
}