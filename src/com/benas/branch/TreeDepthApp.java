package com.benas.branch;

import java.util.Collections;
import java.util.List;

public class TreeDepthApp {
    public static void main(String[] args) {

        //create first node / root branch with parent value null
        Branch rootBranch = new Branch(null);

        //add more children nodes and set parent values
        rootBranch.addChildren(3, rootBranch);
        rootBranch.getChildren().get(2).addChildren(4, rootBranch.getChildren().get(2));
        rootBranch.getChildren().get(2).getChildren().get(1).addChildren(2, rootBranch.getChildren().get(2).getChildren().get(1));
        rootBranch.getChildren().get(2).getChildren().get(1).getChildren().get(0).addChildren(5, rootBranch.getChildren().get(2).getChildren().get(1).getChildren().get(0));

        //collect and add all leafs to list
        rootBranch.collectAllLeafs(rootBranch);

        //get list with all leafs
        List<Branch> leafs = rootBranch.getLeafs();

        //calculate and get list with depths, find max value and print it out
        List<Integer> theLeafsDepths = rootBranch.calculateDepth(leafs);
        System.out.println("\nMax depth: " + Collections.max(theLeafsDepths));
    }
}
